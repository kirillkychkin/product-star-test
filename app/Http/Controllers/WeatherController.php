<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WeatherController extends Controller
{
    /**
     * Request weather from
     * Yandex Weather API
     */
    private function requestWeather($coords)
    {
        $response = Http::withHeaders([
            'X-Yandex-API-Key' => env('X_Yandex_API_Key')
        ])->get('https://api.weather.yandex.ru/v2/forecast', [
            'lat' => $coords['lat'],
            'lon' => $coords['lon'],
            'lang' => 'ru_RU'
        ]);

        return $response->json();
    }

    /**
     * Show weather view
     */
    //https://api.weather.yandex.ru/v2/informers?lat=68.94&lon=33.07  
    public function show(Request $request)
    {
        /**
         * If we need to get another
         * location weather, just change
         * these parameters
         */
        $coords = [
            'lat' => 68.970663,
            'lon' => 33.074909,
        ];

        /**
         * Perform request with custom coords
         */
        $weather = WeatherController::requestWeather($coords);

        return view('weather')
            ->with('city', $weather['geo_object']['locality']['name'])
            ->with('currTemp', $weather['fact']['temp'])
            ->with('feelsLike', $weather['fact']['feels_like'])
            ->with('humidity', $weather['fact']['humidity'])
            ->with('windSpeed', $weather['fact']['wind_speed'])
            ->with('uvIndex', $weather['fact']['uv_index']);
    }
}
